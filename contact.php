<?php include_once "header.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="contact.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="header.css">
</head>
<body>
    <header><h2><b>Drop Us A Line!</b></h2></header>

    <p>Have a question or comment? Use the form below to send us a message or contact us by mail</p>

    <div id="contacts">

        <label for="service"></label>
        <select type="text" id="service" name="Customer servise" placeholder="Customer service">  
            <option value="exemple1">exemple</option>
            <option value="exemple2">exemple</option>
            <option value="exemple3">exemple</option>
        </select>

        <label for="file"></label>
        <input type="file" id="file" name="file">

        <label for="email"></label>
        <input type="email" id="email" name="email" placeholder="your@email.com">

        <label for="help"></label>
        <textarea id="help" name="help" placeholder="How can we help?" style="height:200px"></textarea>

        <input type="button" onclick="MyFunction()" value="Submit" >
    </div>
    <script src="contact.js">
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = function() {
        const myObj = JSON.parse(this.responseText);
        document.getElementById("email").innerHTML = myObj[3];
         }
        xmlhttp.open("GET", "check-email.php");
        xmlhttp.send();
    </script>
    
</body>
<?php include_once "footer.php"?>